## v0.0.6 (2020-11-08)

### Fixed
* b6a4817 -  added feature A5
## v0.0.5 (2020-11-08)

### Added
* 5e9e173 -  Some other fixes
### Fixed
* fba0099 -  Added new feature
* f80c6f4 -  Feature added
* 9685f96 -  added feature
* d668ad2 -  Added another feature
### Others
* b03ecd0 -  v
## v0.0.1 (2020-11-08)

### Updated
* eacc233 -  Update .gitlab-ci.yml
* afa65f6 -  Update .gitlab-ci.yml
* 3b8a528 -  Update .gitlab-ci.yml
* e2c2c7c -  Update README.md
* c29fc5c -  Update .gitlab-ci.yml
* d46d5ac -  Update README.md
* 69124aa -  Update .gitlab-ci.yml
* 0444989 -  Update .gitlab-ci.yml
* a6f9477 -  Updated readme
* cd5ef4b -  updated cicd scripts
* 24953d1 -  updated cicid
### Fixed
* bbef8ad -  Add .gitlab-ci.yml
* 1d7cbbb -  added git ignore
* 67198a0 -  added cicd
### Others
* a6352b1 -  Release version 0.0.0
* 0a10816 -  Release version 3.9.8
* 67e707f -  0.0.1
* 9ab83fd -  Set initial version
* 12d8157 -  a
* 7db2aba -  increased v
