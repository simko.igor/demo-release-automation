#!bin/bash

# 2. discover latest version from dev branch
git checkout origin/dev -- VERSION

dev_branch_name="dev"
version=`cat VERSION | sed -e 's/-SNAPSHOT//'`

# 2.1 switch to release (should be created in previous step 1_start_release.sh)
git checkout -f release/$version

# 2.2 freeze version
echo "$version" > VERSION
git tag "v$version"

# 2.3 generate CHANGELOG.md from git log
./generate_changelog.sh > CHANGELOG.md

# 2.4 push changes to remote
git add .
git commit -m "Freezed v$version and Auto-generated CHANGELOG.md"
git push release/$version

# 2.4 merge release branch to origin/master
git checkout -f master
git merge release/$version -m "Release v$version"

# 2.5 increase version and add -SNAPSHOT to VERSION file
git checkout -f release/$version
echo "$version-SNAPSHOT" > VERSION
./increase_version.sh

# 2.6 merge release branch to origin/dev
git add .
git commit -m "Start new version $version-SNAPSHOT"
git push release/$version