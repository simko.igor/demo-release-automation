#!/bin/bash
previous_tag=0

function create_issue_link {
  # replace with custom logic (e.g [TASK-123](https://example.com/TASK-123))
  echo ""

  # issue_key=`echo "$1" | sed -e 's/\(PD-[[:digit:]]\+\)\(.*\)/\1 /'`
  # echo "[$issue_key](https://jira.alteso.at/issue/$issue_key"
}  

for current_tag in $(git tag --sort=-creatordate); do
  if [ "$previous_tag" != 0 ]; then
    tag_date=$(git log -1 --no-merges --pretty=format:'%ad' --date=short ${previous_tag})

    ADDED=""
    UPDATED=""
    FIXED=""
    OTHERS=""

    printf "## ${previous_tag} (${tag_date})\n\n"

    changes=$(git log ${current_tag}...${previous_tag} --no-merges --pretty=format:'%Cred%h%Creset -%Creset %s' --abbrev-commit --reverse)

    IFS=$'\n' read -d '' -r -a changes_list <<<"$changes"

    for change in "${changes_list[@]}"; do
      lower_message=`echo "$change" | sed -e 's/\(.*\)/\L\1/'`
      story_link=`create_issue_link $change`

      # Example of replacement
      #   from "eecc233 - Implemented something cool" to "eecc233 - [TASK-123](https://example.com/TASK-123) Implemented something cool"
      # message=`sed -e "s/\(.* -\)\(\s\)\(.*\)/\1 $story_link \3/" <<< $change`

      message=`sed -e "s@\(.* -\)\(\s\)\(.*\)@\1 $story_link \3@" <<< $change`

      if [[ $lower_message == *"fix"* ]]; then
        ADDED+="* $message\n"
      elif [[ $lower_message == *"update"* ]]; then
        UPDATED+="* $message\n"
      elif [[ $lower_message == *"add"* ]] || [[ $lower_message == *"implement"* ]]; then
        FIXED+="* $message\n"
      else
        OTHERS+="* $message\n"
      fi
    done

    if [ ! -z "$ADDED" ]; then
      printf "### Added\n"
      printf "$ADDED"
    fi
    if [ ! -z "$UPDATED" ]; then
      printf "### Updated\n"
      printf "$UPDATED"
    fi
    if [ ! -z "$FIXED" ]; then
      printf "### Fixed\n"
      printf "$FIXED"
    fi
    if [ ! -z "$OTHERS" ]; then
      printf "### Others\n"
      printf "$OTHERS"
    fi
  fi
  previous_tag=${current_tag}
done
