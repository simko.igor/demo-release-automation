#!bin/bash

git checkout origin/dev -- VERSION

dev_branch_name="dev"
version=`cat VERSION | sed -e 's/-SNAPSHOT//'`

# 1. create release branch from origin/dev
git checkout -b release/$version origin/$dev_branch_name

# 1.1 push to remote
git push -u origin release/$version