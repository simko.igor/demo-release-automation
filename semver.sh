#!/bin/bash

if [ -f "VERSION" ]; then
    current_version=$(cat VERSION)
else
    current_version="0.0.0"
fi

version_regex="\([[:digit:]]\+\).\([[:digit:]]\+\).\([[:digit:]]\+\)\(.*\)"

function increase_major_version {
    major_version=$(sed -e "s/$version_regex/\1/" <<< $current_version)
    next_major_version=$((major_version+1))
    retval=$(sed -e "s/$version_regex/$next_major_version.0.0\4/" <<< $current_version)
    echo "$retval"
}
function increase_minor_version {
    minor_version=$(sed -e "s/$version_regex/\2/" <<< $current_version)
    next_minor_version=$((minor_version+1))
    retval=$(sed -e "s/$version_regex/\1.$next_minor_version.0\4/" <<< $current_version)
    echo "$retval"
}
function increase_patch_version {
    patch_version=$(sed -e "s/$version_regex/\3/" <<< $current_version)
    next_patch_version=$((patch_version+1))
    retval=$(sed -e "s/$version_regex/\1.\2.$next_patch_version\4/" <<< $current_version)
    echo "$retval"
}

next_version=""

if [[ $1 == 'increase-major' ]]; then
    next_version=`increase_major_version`
elif [[ $1 == 'increase-minor' ]]; then
    next_version=`increase_minor_version`
elif [[ $1 == 'increase-patch' ]]; then
    next_version=`increase_patch_version`
else
    printf "\nUsage: semver.sh <operation>\n\n"
    printf "  where <operation>:\n\n"
    printf "    increase-major\tincrease major version in VERSION file (e.g. from 1.2.4 to 2.0.0)\n"
    printf "    increase-minor\tincrease minor version in VERSION file (e.g. from 1.2.4 to 1.3.0)\n"
    printf "    increase-patch\tincrease patch version in VERSION file (e.g. from 1.2.4 to 1.2.5)\n"
fi

if [[ ! -z $next_version ]]; then
    echo "$next_version" > VERSION
    echo "Version increased from $current_version to $next_version"
fi